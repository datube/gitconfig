# gitconfig

A `.gitconfig` containing colors, aliases and some other "default" settings;
use as you see fit.

See [git-reference](https://git-scm.com/docs) for more information.


## installation

Copy `.gitconfig` to your `$HOME` and configure:

```bash
cp .gitconfig ~/.
```

```bash
git config --global user.email "you@example.com"
git config --global user.name "Your Name"
```


## aliases

| alias | git command |
| --- | --- |
| br | branch |
| bra | branch -a |
| brd | branch -d |
| brdr | push origin --delete |
| co | checkout |
| ls | ls-files |

| alias | description |
| --- | --- |
| accept \<file> | accepts and adds remote \<file> in case of merge conflict |
| alias | lists aliases |
| [find](#find) | find commits for file or given string  |
| [from](#from) | show commits for given user |
| [hist](#hist) | formatted log; either all for for given file |
| [last](#last) | last x number of commits (default 10) |
| lb | list branches |
| st | simplified status |


### find

* find revision(s) based on a word/sentence

```
git find "this"
git find "or that"
```

* find revision(s) which contains file

```bash
git find README.md
```


### from

```bash
git from
git from -- 20
git from "someone else"
git from someone 20
```


### hist

* formatted log

```bash
git hist
```

* formatted log for specific file

```bash
git hist README.md
```


### last

```bash
git last
git last 20
```
